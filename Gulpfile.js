var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('scss-to-css', function() {
    gulp.src('sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./public/stylesheets/'))
});

//Watch task
gulp.task('default',function() {
    gulp.watch('sass/**/*.scss',['scss-to-css']);
});
