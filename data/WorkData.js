/* Our work pages variables */
var columize = {
	title: 'Columize',
  description: "Columize is your public feed of links. It enables you to share any URL to your Column—a social reading list of sort.",
	why: "When you see something beautiful or cool, you post it to Instagram. Where do you post when you read something interesting? Facebook? Twitter? We felt these networks were not good enough for all these interesting links you stumble upon every day. Columize is your public feed of links. It enables you to share any link to a public feed named a Column. The idea is to make Columize the simplest way to share a link.",

	timeline: [
		"Status: 1.0 live on the <a href='https://itunes.apple.com/us/app/columize/id1093561023?mt=8'>App Store</a>",
		"Timeline: March 2016 - Present"
	],

	role: [
		"UX, UI, engineering"
	],

	features: [
		"People can only post links, no plaintext posts, no images (more signal, less noise)",
		"Public Feed emulating Twitter's public timeline",
		"Google AMP integration",
	],
	ref: 'work/columize',
	numberOfPicturesInGallery: 3,
	detail: '<a href="https://itunes.apple.com/us/app/columize/id1093561023?mt=8">App Store</a> · Version 1.0'
};

var emercall = {
	title: 'Emercall',
  description: "Get emergency service phone numbers wherever you are in the world.",
	why: "One day, Axel — cofounder of Kuma and full-stack developer — was in Italy on a romantic trip. An emergency came up and he didn't have the number for local emergency services, nor did he have an Internet connection. Millenials like us can be a bit lost in such situations. That's why we decided to build Emercall. Emercall uses your GPS coordinates to get you the number for police, medical and fire emergency services in any country in the world.",

	timeline: [
		"Status: 1.0 live on the <a href='https://itunes.apple.com/us/app/emercall/id1124101485?mt=8'>App Store</a>",
		"Timeline: June 2016 - Present"
	],

	role: [
		"UX, UI, engineering"
	],
	ref: 'work/emercall',
	numberOfPicturesInGallery: 2,
	detail: '<a href="https://itunes.apple.com/us/app/emercall/id1124101485?mt=8">App Store</a> · Version 1.0'
};

var qumblr = {
	title: 'Qumblr',
  description: "Simple Tumblr GIF and image search engine.",
	why: "We wanted to try our AngularJS chops with this simple web app that gives you an interface to search for Tumblr's evergreen images and GIFs. Being big fans of Stevie Ray Vaughan and Led Zeppelin, choosing Tumblr as the source for images made a lot of sense.",

	timeline: [
		"Status: 1.0 live <a href='http://qumblr.com/'>website</a>",
		"Timeline: March 2016 - Present"
	],

	role: [
		"UX, UI, engineering"
	],

	features: [
		"Responsive web app",
		"Infinite scroll",
		"One-click download",
	],
	ref: 'work/qumblr',
	numberOfPicturesInGallery: 2,
	detail: '<a href="http://qumblr.com">Website</a> · Version 1.0'
};

var maison = {
	title: 'MAISON',
  description: "MAISON is the best burger joint in Paris. They asked us to redesign their website to be as simple and thoughtful as their menu.",
	why: "The best burger joint in Paris asked us to build a simple, elegant “business card” like website. Something sober and thoughtful, just like their menu. So we came up with this simple responsive design giving people the most important information really fast.",

	timeline: [
		"Status: 1.0 live <a href='http://www.maisonburger.com/'>website</a>",
		"Timeline: June 2016 - Present"
	],

	role: [
		"UX, UI, engineering"
	],

	ref: 'work/maison',
	numberOfPicturesInGallery: 2,
	detail: '<a href="http://www.maisonburger.com/">Website</a> · Version 1.0'
};


var loop = {
	title: 'Loop',
  description: "Columize is an iOS app that lets you publicly post your feed of links.",
	introduction: "Group photo-sharing tools lacked a certain je-ne-sais-quoi. Photos drowned in IMs. iCloud Photo Sharing being email-based. Considering the importance of photos—the cornerstone of our collective memory—especially in a mobile world, we set out to build a simple way to share photos among your friends or family.",
	information: [
		{key: 'Kuma\'s role', content: 'product management, UX/UI design, branding'},
		{key: 'Status', content: 'discontinued'},
		{key: 'Timeline', content: 'March 2014 - September 2015'},
		{key: 'Detailed thought process', content: '<a href="https://portfolio.ulysse.works/loop-for-ios-87c6e458d209#.5i5hoq3ne">Read on Medium</a>'}
	],
	features: [
		"iOS app",
		"Create albums by selecting people from your phone contact list",
		"Photos are neatly laid out and always accessible—not lost in sea of IMs",
		"Only the people you invite to your albums can view, share and comment on photos"
	],
	ref: 'https://portfolio.ulysse.works/loop-for-ios-87c6e458d209#.ckni7debl',
	numberOfPicturesInGallery: 5,
};

var foound = {
	title: 'Foound',
  description: "Columize is an iOS app that lets you publicly post your feed of links.",
	introduction: "Foound is a yet unreleased app that displays an aggregated list of all the restaurants around you. It takes its information from a wealth of sources, such as TripAdvisor, Foursquare and Yelp. The original idea came about when the gluten-intolerant founder thought technology should help him find a place to eat, no matter where he was.",

	information: [
		{key: 'Kuma\'s role', content: 'frontend development,product management, UI/UX'},
		{key: 'Status', content: 'unreleased'},
		{key: 'Timeline', content: 'September 2015 - February 2016'},
		{key: 'Detailed thought process', content: '<a href="https://portfolio.ulysse.works/foound-for-ios-b9b00e93d02a">Read on Medium</a>'}
	],
	features: [
		"iOS app",
		"Look for all restaurants around you—really all of them",
		"Refine your search with filters to find the right restaurant",
		"Search any restaurant around the world and get relevant information"
	],
	ref: 'https://portfolio.ulysse.works/foound-for-ios-b9b00e93d02a?source=featured',
	numberOfPicturesInGallery: 3,
};

var data = {};

data.current = {"columize": columize, "emercall": emercall, "qumblr": qumblr, "maison": maison};
data.previous =  {"loop":loop, "foound": foound};

module.exports = data
