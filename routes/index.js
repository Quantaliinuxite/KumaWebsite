var express = require('express');
var router = express.Router();
var nodemailer = require('nodemailer');
var projects = require('../data/WorkData.js');

var transporter = nodemailer.createTransport({
	name: 'kuma-comment'
});

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Kuma' });
});

router.get('/about', function(req,res,next){
	res.render('about', { title: 'About' });
});

router.get('/contact', function(req,res,next) {
	res.render('contact', {title: 'Contact'});
});

router.get('/work/:project', function(req,res,next){
	var project = projects.current[req.params.project];
	if(project){
		res.render('work-template',project)
	}
	else {
			var error = new Error("Could not find this project!");
			next(error);
	}
});

router.get('/work', function(req,res,next) {
	console.log(projects);
	res.render('work', {title: 'Work', projects: projects});
});

router.post('/contact', function(req,res,next){
	var mailOptions = {
		from: '<noreply@kuma-comment.studio>',
		to: 'ulyssesabbag@gmail.com, axelltk@gmail.com',
		subject: 'Comment From Kuma: '+req.body.email,
		text: req.body.message
	};

	transporter.sendMail(mailOptions,function(err,info) {
		if(err) {
			return console.log(err);
		}
		res.render('contact-success', {title: 'Succes'});
		console.log('Message sent: ' + info.response);
	});
});

module.exports = router;
